package sslogs

import (
	"container/ring"
	"fmt"
	"log"
	"sync"
	"time"
)

const fs = "%-7s %v\n"
const bufferSize = 25

var level int

//These constants represent the logging levels available
const (
	MIN   = 1
	DEBUG = 1
	INFO  = 2
	ERROR = 3
	MAX   = 3
)

type logBuffer struct {
	sync.RWMutex
	*ring.Ring
}

var buffer logBuffer

//Error log call
func Error(m string) {
	s := fmt.Sprintf(fs, "[ERROR]", m)
	logItem(ERROR, s)
}

//Errorf Formats arguments to be passed into the log
func Errorf(fs string, args ...interface{}) {
	Error(fmt.Sprintf(fs, args...))
}

//Info log call
func Info(m string) {
	s := fmt.Sprintf(fs, "[INFO]", m)
	logItem(INFO, s)
}

//Infof formats arguments to be passed to the info log
func Infof(fs string, args ...interface{}) {
	Info(fmt.Sprintf(fs, args...))
}

//Debug log call
func Debug(m string) {
	s := fmt.Sprintf(fs, "[DEBUG]", m)
	logItem(DEBUG, s)
}

//Debugf formats arguments to be passed to the debug log
func Debugf(fs string, args ...interface{}) {
	Debug(fmt.Sprintf(fs, args...))
}

func logItem(l int, m string) {
	if buffer.Ring == nil {
		buffer.Ring = ring.New(bufferSize)
	}
	if l >= level {
		log.Print(m)
		m = fmt.Sprintf("%s %s", time.Now(), m)
		buffer.Lock()
		buffer.Value = m
		buffer.Ring = buffer.Next()
		buffer.Unlock()
	}
}

//SetLogLevel sets the logging level for the application
func SetLogLevel(l int) {
	if l < MIN {
		l = MIN
	}
	if l > MAX {
		l = MAX
	}
	level = l
	buffer.Ring = ring.New(bufferSize)
}

//DumpBuffer will return a byte slice of all entries in the log ring buffer
func DumpBuffer() []byte {
	buffer.RLock()
	n := buffer.Next()
	lines := ""
	for i := 0; i < bufferSize; i++ {
		if n.Value != nil {
			lines += n.Value.(string)
		}
		n = n.Next()
	}
	buffer.RUnlock()
	return []byte(lines)
}
